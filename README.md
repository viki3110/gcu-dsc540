
## Import all the required packages 

import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import seaborn as sns
import numpy as np
from scipy.stats import shapiro     #normality test
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split, cross_val_predict
from sklearn.metrics import mean_squared_error, r2_score
from scipy.spatial import distance
from sklearn.decomposition import PCA
from yellowbrick.regressor import ResidualsPlot 
from mpl_toolkits.mplot3d import Axes3D   #plot 3D
from hdbscan import HDBSCAN
from statsmodels.stats.stattools import durbin_watson
import statsmodels.api as sm
import statsmodels.stats.api as sms
from sklearn import metrics
from sklearn.feature_selection import VarianceThreshold
from sklearn.decomposition import PCA
from sklearn.model_selection import cross_val_score
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier
import h5py
from collections import Counter
import time
from sklearn.neighbors import kneighbors_graph
from sklearn.metrics import confusion_matrix
from sklearn.metrics import plot_confusion_matrix
from sklearn import tree
from sklearn.metrics import classification_report

from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score
from sklearn.metrics import auc



---
## Import the data 

image_trn= pd.read_csv("/Users/vikashbhagat/DSC-540/mnist_train.csv")

image_tst=pd.read_csv("/Users/vikashbhagat/DSC-540/mnist_test.csv")
 
 
    
## Classify the train and test data


Y_train=np.array(image_trn.iloc[:,0]) # Getting the target  from the image_trn

X_train=np.array(image_trn.iloc[:,1:]) # Getting the predictors from the image_trn



Y_test=np.array(image_tst.iloc[:,0])

X_test=np.array(image_tst.iloc[:,1:])

## Run the below code to generate show image function

size_img = 28
threshold_color = 100 / 255

def show_imag(x):
    plt.figure(figsize=(8,7))
    if x.shape[0]>100 :
        print(x.shape[0])
        n_imgs = 16
        n_samples = x.shape[0]
        x = x.reshape(n_samples, size_img, size_img)
        for i in range(16):
            plt.subplot(4, 4, i+1) #devide figure into 4x4 and choose i+1 to draw
            plt.imshow(x[i])
        plt.show()
    else:
        plt.imshow(x)
        plt.show()
        
    
## Feature selection


def get_pca(x_train, x_test):
    pca = PCA(n_components=0.95)
    pca.fit(x_train)
    x_train = pca.transform(x_train)
    x_test = pca.transform(x_test)
    print(x_train.shape, x_test.shape)
    return x_train, x_test
    
    

 #Scaling the data
from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
   
  # Fit on training set only.
scaler.fit(X_train)

# Apply transform to both the training set and the test set.
X_train = scaler.transform(X_train)
X_test = scaler.transform(X_test)


# apply PCA to the data
from sklearn.decomposition import PCA
pca = PCA(.95)
pca.fit(X_train)
X_train = pca.transform(X_train)
X_test = pca.transform(X_test)


# KNN Algorithm
from sklearn.neighbors import KNeighborsClassifier
knn = KNeighborsClassifier(n_neighbors=7,n_jobs=-1)
#,n_jobs=-1
# Traning the model.
knn.fit(X_train,Y_train)

# Making Prediction
y_pred = knn.predict(X_test)
        





# Print the confusion matrix
print(confusion_matrix(Y_test,y_pred))



# Print the Classification report 
print(classification_report(Y_test,y_pred))




# DO the ROC curve


plt.plot(fpr[0], tpr[0], linestyle='--',color='orange', label='0 vs rest')
plt.plot(fpr[1], tpr[1], linestyle='--',color='green', label='1 vs Rest')
plt.plot(fpr[2], tpr[2], linestyle='--',color='blue', label='2 vs Rest')
plt.plot(fpr[3], tpr[3], linestyle='--',color='black', label='3 vs rest')
plt.plot(fpr[4], tpr[4], linestyle='--',color='yellow', label='4 vs Rest')
plt.plot(fpr[5], tpr[5], linestyle='--',color='blue', label='5 vs Rest')
plt.plot(fpr[6], tpr[6], linestyle='--',color='cyan', label='6 vs rest')
plt.plot(fpr[7], tpr[7], linestyle='--',color='magenta', label='7 vs Rest')
plt.plot(fpr[8], tpr[8], linestyle='--',color='blue', label='8 vs Rest')
plt.plot(fpr[9], tpr[9], linestyle='--',color='black', label='9 vs Rest')

plt.title('Multiclass MNIST Digit data ROC curve')
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive rate')
plt.legend(loc='best')
plt.savefig('Multiclass ROC',dpi=300);

